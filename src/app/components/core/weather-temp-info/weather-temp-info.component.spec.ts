import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherTempInfoComponent } from './weather-temp-info.component';

describe('WeatherTempInfoComponent', () => {
  let component: WeatherTempInfoComponent;
  let fixture: ComponentFixture<WeatherTempInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherTempInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherTempInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
