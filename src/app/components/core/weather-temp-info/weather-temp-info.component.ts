import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-temp-info',
  templateUrl: './weather-temp-info.component.html',
  styleUrls: ['./weather-temp-info.component.sass']
})
export class WeatherTempInfoComponent implements OnInit {

  @Input() temp;

  constructor() { }

  ngOnInit() {
  }

}
