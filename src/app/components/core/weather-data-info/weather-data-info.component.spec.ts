import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherDataInfoComponent } from './weather-data-info.component';

describe('WeatherDataInfoComponent', () => {
  let component: WeatherDataInfoComponent;
  let fixture: ComponentFixture<WeatherDataInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherDataInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherDataInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
