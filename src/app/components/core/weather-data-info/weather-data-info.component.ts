import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-data-info',
  templateUrl: './weather-data-info.component.html',
  styleUrls: ['./weather-data-info.component.sass']
})
export class WeatherDataInfoComponent implements OnInit {

  @Input() pressure;
  @Input() humidity;
  @Input() windVel;

  constructor() { }

  ngOnInit() {
  }

}
