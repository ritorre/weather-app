import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-city-info',
  templateUrl: './weather-city-info.component.html',
  styleUrls: ['./weather-city-info.component.sass']
})
export class WeatherCityInfoComponent implements OnInit {

  @Input() cityName;
  @Input() cityDate;

  constructor() { }

  ngOnInit() {
  }

}
