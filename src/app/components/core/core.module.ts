import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherDayInfoComponent } from './weather-day-info/weather-day-info.component';
import { WeatherIconComponent } from './weather-icon/weather-icon.component';
import { WeatherMessageComponent } from './weather-message/weather-message.component';
import { WeatherCityInfoComponent } from './weather-city-info/weather-city-info.component';
import { WeatherTempInfoComponent } from './weather-temp-info/weather-temp-info.component';
import { WeatherDataInfoComponent } from './weather-data-info/weather-data-info.component';
import { TempConverterPipe } from '../../pipes/temp-converter.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TempConverterPipe,
    WeatherDayInfoComponent,
    WeatherIconComponent,
    WeatherMessageComponent,
    WeatherCityInfoComponent,
    WeatherTempInfoComponent,
    WeatherDataInfoComponent
  ],
  exports: [
    WeatherDayInfoComponent,
    WeatherIconComponent,
    WeatherMessageComponent,
    WeatherCityInfoComponent,
    WeatherTempInfoComponent,
    WeatherDataInfoComponent
  ]
})
export class CoreModule { }
