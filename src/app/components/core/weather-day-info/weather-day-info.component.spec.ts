import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherDayInfoComponent } from './weather-day-info.component';

describe('WeatherDayInfoComponent', () => {
  let component: WeatherDayInfoComponent;
  let fixture: ComponentFixture<WeatherDayInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherDayInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherDayInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
