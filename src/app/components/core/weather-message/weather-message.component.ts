import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-message',
  templateUrl: './weather-message.component.html',
  styleUrls: ['./weather-message.component.sass']
})
export class WeatherMessageComponent implements OnInit {

  @Input() msg;

  constructor() { }

  ngOnInit() {

  }

}
