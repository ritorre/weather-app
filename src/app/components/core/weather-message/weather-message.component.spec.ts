import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherMessageComponent } from './weather-message.component';

describe('WeatherMessageComponent', () => {
  let component: WeatherMessageComponent;
  let fixture: ComponentFixture<WeatherMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
