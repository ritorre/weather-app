import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../../services/weather.service';

import { Ng4LoadingSpinnerModule, Ng4LoadingSpinnerService  } from 'ng4-loading-spinner';

interface FormInfo {
  cityName: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  data: object;
  error;
  formInfo: FormInfo = {
    cityName: ''
  };

  constructor(
    private weatherServ: WeatherService,
    private ng4LoadingSpinnerService: Ng4LoadingSpinnerService
  ) { }

  ngOnInit() {}

  /**
   *  method findCityByName
   *  Method to search a city weather
   *
   * @memberof HomeComponent
   */
  findCityByName() {
    this.ng4LoadingSpinnerService.show();
    this.weatherServ.getWeatherByCity(this.formInfo.cityName)
      .subscribe(data => {
        this.data = data;
        this.ng4LoadingSpinnerService.hide();
      },
        err => {
          err = this.error;
          this.ng4LoadingSpinnerService.hide();
        });
  }

}
