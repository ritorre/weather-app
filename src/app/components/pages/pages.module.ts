import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { CoreModule } from '../core/core.module';
import { PanelComponent } from './panel/panel.component';
import { WeatherPanelComponent } from './weather-panel/weather-panel.component';
import { FormsModule } from '@angular/forms';

import { Ng4LoadingSpinnerModule, Ng4LoadingSpinnerService  } from 'ng4-loading-spinner';


@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    Ng4LoadingSpinnerModule
  ],
  declarations: [
    HomeComponent,
    PanelComponent,
    WeatherPanelComponent
  ],
  exports: [
    HomeComponent,
    PanelComponent,
    WeatherPanelComponent
  ]
})
export class PagesModule { }
