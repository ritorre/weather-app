import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-panel',
  templateUrl: './weather-panel.component.html',
  styleUrls: ['./weather-panel.component.sass']
})
export class WeatherPanelComponent implements OnInit {

  @Input() temperature;
  @Input() weatherIcon;
  @Input() pressure;
  @Input() humidity;
  @Input() windVel;
  @Input() msg;

  constructor() { }

  ngOnInit() {
  }

}
