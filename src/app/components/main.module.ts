import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from './core/core.module';
import { HomeComponent } from './pages/home/home.component';
import { PagesModule } from './pages/pages.module';

@NgModule({
  imports: [
    CommonModule,
    PagesModule
  ],
  exports: [
    PagesModule
  ],
  declarations: []
})
export class MainModule { }
