import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tempConverter'
})
export class TempConverterPipe implements PipeTransform {

  transform(kelvin: number): string {
    return (kelvin - 273.15).toString();
  }

}
