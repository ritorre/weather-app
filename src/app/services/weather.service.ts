import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const BASE_URL: string = environment.BASE_URL;
const API_TOKEN: string = environment.API_TOKEN;

@Injectable()
export class WeatherService {

  constructor(private http: HttpClient) { }

  /**
   * method getWeatherByCity
   * Method to obtain a city weather by param name
   *
   * @param {string} city
   * @returns object
   * @memberof WeatherService
   */
  getWeatherByCity(city: string) {
    return this.http.get(`${BASE_URL}?q=${city.toLowerCase()}&APPID=${API_TOKEN}`);
  }
}
