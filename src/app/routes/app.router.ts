import { routes } from './routes';

import { RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

// Export the router with the router module
export const appRouter: ModuleWithProviders = RouterModule.forRoot(routes);
