import { Routes } from '@angular/router';
import { HomeComponent } from '../components/pages/home/home.component';

// routes -> Application routes
export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: '**', redirectTo: '' }
];
